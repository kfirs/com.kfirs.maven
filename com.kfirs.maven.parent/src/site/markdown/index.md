## Welcome to the KFIRS Maven Parent POM project

This module provides the KFIRS parent POM file for use in Apache Maven.

Our parent POM extends Spring Boot's parent POM, which enables us to
easily get many of the sensible defaults the Spring folks kindly shared
with the world, as well as apply many of our own:

* Updated Maven plugins
* Easy analysis of unused & declared dependencies, used & undeclared ones, etc
* Enforcement of certain development rules (eg. forbid `commons-logging`)
* Shared distribution management for artifacts

Note that even though it extends Spring Boot, it does not force you to
actually use Spring Boot, nor does it add **any** of its dependencies
onto your project (or any dependencies what-so-ever for that fact).

### Usage ###

To extend this parent POM, add this to your POM:

    <parent>
        <groupId>com.kfirs.maven</groupId>
        <artifactId>com.kfirs.maven.parent</artifactId>
        <version>SET_VERSION_HERE</version>
        <relativePath/>
    </parent>

And you're ready to go! See [the usage page](usage.html) for more information.