## Usage

The KFIRS parent POM provides several useful _inherited_ facilities and
allows for some useful workflows, all detailed in this document (WIP).

The use this POM as your parent, set this in your POM:

    <parent>
        <groupId>com.kfirs.maven</groupId>
        <artifactId>com.kfirs.maven.parent</artifactId>
        <version>SET_VERSION_HERE</version>
        <relativePath/>
    </parent>

### Compiler

Compiler configuration is set by default to:

* Add debug information (lines,vars,source)
* Show general & deprecation warnings
* Language level set to the `${java.version}` property, so you can modify
  it to whatever target bytecode version you are setting as a minimum.
* Adds the `-parameters` compiler argument by default so Java8 parameter
  names are added to the bytecode.

### Dependency management

Aside from getting Spring's dependency management (since it is used as
a parent POM), this plugin binds the `maven-dependency-plugin`'s
`analyze-only` goal to the lifecycle, printing a report (but NOT failing
the build!) of dependencies that are used but not declared, declared but
not used, etc. It is configured to ignore the `spring-boot-configuration-processor`
and any `spring-boot-starter-*` dependencies in that report.

### Validation rules

This plugin will ensure that:

* The `commons-logging` dependency is not imported either directly or indirectly.
* The JDK version used is 1.8.
* The Maven version is at least 3.3.9 but not 4.x
* There are no `<repository>` declarations in the POM
* Versions are declared for all `<plugin>` usages
* All Spring Framework dependencies share the exact same version
* All Spring Boot dependencies share the exact same version
* All Spring Security dependencies share the exact same version
* All SLF4J dependencies share the exact same version
* All Logback dependencies share the exact same version

### Artifact generation

### JARs

The `maven-jar-plugin` is configured NOT to compress the resulting JAR,
and NOT to add an index.

### Site generation

This POM adds the `org.apache.maven.wagon`:`wagon-ssh-external` global
extension, which allows SSH/SCP deployment for all plugins, but its
specifically useful for site deployment (via the `org.apache.maven.plugins`:`maven-site-plugin`).

#### Javadoc

The `maven-javadoc-plugin` is configured to support the following javadoc
tags:

* `@on`
* `@todo`
* `@review`
* `@task`
* `@fixme`
* `@bug`
* `@upgrade`
* `@test`

#### Project info reports

The project information reports are *NOT* inherited, requiring each
extending project & module to define the reports it wants. I've found
this method much more reliable when used with Maven inheritance since
each module type usually needs different reports.

### Testing

The `maven-surefire-plugin` is configured to:

* Add the AspectJ agent to the commandline, thus enabling aspects to
  run in tests, just like your production code.
* When the `coverage` profile is active, the `JaCoCo` agent is also
  added, measuring the test coverage. The `jacoco-maven-plugin` plugin
  will then generate a report that will be included in the resulting
  site.
* Use only one fork with one thread, to be reused serially. Output from
  the fork is directed to a file with no stacktrace modification.
* The `useSystemClassLoader` feature of surefire is *disabled* since it
  causes issues when used with service loaders and other classloader
  thingies.
* The fork is given a unique working directory with the fork number in
  its name, so if you increase the forks count, each one uses its own
  working directory.

### Deployment

The `maven-install-plugin` & `maven-deploy-plugin` are configured to
install/deploy artifacts _at the end_
of a successful build.

### Releasing

The `jgitflow-maven-plugin` is configured to sensible Gitflow logic:

* Pull changes before running
* Push changes after running
* Will NOT squash commits
* Submodule and dependency versions will be updated properly.
* Release will only `verify` and not deploy, leaving that part to your
  CI framework to do instead.
