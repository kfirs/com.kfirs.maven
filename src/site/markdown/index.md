## Welcome to the KFIRS Maven project

This project provides the fundemental pieces for building all KFIRS
project - specifically, the KFIRS parent POM file for Apache Maven.

The parent POM (`com.kfirs.maven`:`com.kfirs.maven.parent`) is extended
by all other KFIRS modules which allows sharing many build related
behaviors from a single place.
